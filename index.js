const express = require("express");
//Mongoose is a package that allows creation of Schemas to model our data structures
//Also has access to a number of methods for manipulation our database
const mongoose = require("mongoose");

const app = express();

const port = 3001;
//MongoDB Atlas Connection
//Connect to the database by passing in your connection string, remember to replace the password and database names with actual values.
//when we want to use local mongoDB/robo3t
//mongoose.connect("mongodb://localhost:27017/databaseName")

mongoose.connect('mongodb+srv://dbUser:dbUser@zuitt.ri5rh.mongodb.net/batch164_to-do?retryWrites=true&w=majority', 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
)
//Due to updates in mongodb drivers that allow connection to it, the default connection string is being flagged as an error
//By default a warning will be displayed in the terminal when the app is running, but this will not prevent mongoose from being used in the app.
//to prevent/avoid any current and future errors while connecting to Mongo DB, add the userNewUrlParser and useUnifiedTopology.

//Set notifications for connection success or failure
let db = mongoose.connection;

//console.error.binde(console) allows us to print errors in the browser console and in the terminal
//"connection error" is the message that will display if an error is encountered.
db.on("error", console.error.bind(console, "connection error"));
//connection successful message
db.once("open", () => console.log("We're connected to the cloud database"))


app.use(express.json());

app.use(express.urlencoded({ extended:true }));



//Mongoose Schemas
//Schemas determine the structure of the documents to be written in the database
//act as blueprint to our data
//Use Schema() constructor of the Mongoose module to create a new Schema object
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		//Default values are the predefined values for a field if we don't put any value.
		default: "pending"
	}
})

// Models
	// Models use Schemas and they act as the middleman from the server (JS code) to our database
	// The variable/object "Task" can now used to run commands for interacting with our database
	// "Task" is capitalized following the MVC approach for naming conventions
	// Models must be in singular form and capitalized
	// The first parameter of the Mongoose model method indicates the COLLECTION in where to store the data
	// The second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
	// Using Mongoose, the package was programmed well enough that it automatically converts the singular form of the model name into a plural form when creating a collection in postman


const Task = mongoose.model("Task", taskSchema);





//Routes/endpoints

//Creating a new task

//Business Logic
/*
1. Add a functionality to check if there are duplicates tasks
	-If the task already exists in the database, we return error
	-If the task doesn't exist in the database, we add it in the database
		1. The task data will be coming from the request's body.
		2. Create a new Task object with field/property
		3. save the new object to our database.
*/

app.post("/tasks", (req, res) => {
	Task.findOne({ name: req.body.name }, (err, result) => {

		// Check if there are duplicate tasks
		// "findOne" is a Mongoose method that acts similar to "find" of MongoDB
		// findOne() returns the first document that matches the search criteria
		// If there are no matches, the value of result is null
		// "err" is a shorthand naming convention for errors

		//If a document was found and the document's name matches the information sent via the client
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found");
		} else{
			//if no document was found
			//create a new task object and save it to the database
			let newTask = new Task({
				name: req.body.name
			});

			// The "save" method will store the information to the database
			// Since the "newTask" was created/instantiated from the Mongoose Schema it will gain access to this method to save to the database
			// The "save" method will accept a callback function which stores any errors found in the first parameter
			// The second parameter of the callback function will store the newly saved document
			// Call back functions in mongoose methods are programmed this way to store any errors in the first parameter and the returned results in the second parameter
			newTask.save((saveErr, savedTask) => {
				//if there are errors in saving
				if(saveErr){

					// Will print any errors found in the console
					// saveErr is an error object that will contain details about the error
					// Errors normally come as an object data type
					return console.error(saveErr)
				} else {
					// Return a status code of 201 for created
					// Sends a message "New task created" on successful creation
					return res.status(201).send("New task created")
				}
			})


		}
	})
})


//Get all tasks
//Business Logic
/*
1. Find/retrieve all the documents
2. if an error is encountered, print the error
3. if no errors are found, send a success status back to the client and return an array of documents


*/
app.get("/tasks", (req, res) => {

	// "find" is a Mongoose method that is similar to Mongodb "find", and an empty "{}" means it returns all the documents and stores them in the "result" parameter of the callback function
	Task.find({}, (err, result) => {

		if(err) {
			return console.log(err);
		} else {
			// Status "200" means that everything is "OK" in terms of processing
			// The "json" method allows to send a JSON format for the response
			// The returned response is purposefully returned as an object with the "data" property to mirror real world complex data structures
			return res.status(200).json({
				Message: "from the database",
				dataFromMDB: result
			})
		}
	})
})


/*
Activity:
1. Create a User schema.
2. Create a User model.
3. Create a POST route that will access the "/signup" route that will create a user.
4. Process a POST request at the "/signup" route using postman to register a user.
5. Create a GET route that will return all users.
6. Process a GET request at the "/users" route using postman.
7. Create a git repository named S30.
8. Initialize a local git repository, add the remote link and push to git with the commit message of s30 Activity.
9. Add the link in Boodle named Express js Data Persistence via Mongoose ODM.


Business Logic in Creating a user "/signup"
1. Add a functionality to check if there are duplicates tasks
	- if the user already exists, return error Or "Already registered"
	- if the user does not exist, we add it on our database
		1. The user data will be coming from the req.body
		2. Create a new User object with a "username" and "password" fields/properties
		3. Then save, and add an error handling



*/

//[Section] Activity
// User Schema/Model
const userSchema = new mongoose.Schema({
    username : String,
    password : String
})

// The variable/object "User" can now used to run commands for interacting with our database
const User = mongoose.model("User", userSchema);

// Registering a user

// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
	- If the user already exists in the database, we return an error
	- If the user doesn't exist in the database, we add it in the database
2. The user data will be coming from the request's body
3. Create a new User object with a "username" and "password" fields/properties
*/

// Route for creating a user
app.post("/signup", (req, res)=> {

	// Finds for a document with the matching username provided in the client/Postman
	User.findOne({ username : req.body.username }, (err, result) => {

		// Check for duplicates
		if(result != null && result.username == req.body.username){

			return res.send("Duplicate username found");

		// No duplicates found
		} else {

			// If the username and password are both not blank
			if(req.body.username !== '' && req.body.password !== ''){

				// Create/instantiate a "newUser" from the "User" model
                let newUser = new User({
                    username : req.body.username,
                    password : req.body.password
                });
    
    			// Create a document in the database
                newUser.save((saveErr, savedTask) => {

                    // If an error occurred
                    if(saveErr){

                    	// Return an error in the client/Postman
                        return console.error(saveErr);

                    // If no errors are found
                    } else {

                    	// Send a response back to the client/Postman of "created"
                        return res.status(201).send("New user registered");

                    }

                })

            // If the "username" or "password" was left blank
            } else {

            	/// Send a response back to the client/Postman of "created"
                return res.send("BOTH username and password must be provided.");
            }			
		}
	})
})






app.listen(port, ()=> console.log(`Server running at port ${port}`));
